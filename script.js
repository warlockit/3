function showTab(event) {
	var nav = this.parentNode.parentNode;
	var li = nav.getElementsByTagName("li");
	var tabs = nav.nextElementSibling.children;
	var active = this.href.split("#")[1]
	event.preventDefault();

	for (var i = 0; i < li.length; i++) {
		if (li[i].classList.contains("active")) li[i].classList.remove("active");
	}

	this.parentNode.classList.add("active");
	
	for (var i = 0; i < tabs.length; i++) {
		if (tabs[i].classList.contains("active")) tabs[i].classList.remove("active");

		if (tabs[i].id == active) tabs[i].classList.add("active");
	}
}

//	Функции для работы с DOM
//	getElementById
//	getElementsByClassName
//	getElementsByTagName
//	document.querySelectorAll('ul > li:last-child');