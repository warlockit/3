function goTop(acceleration, time) {
	acceleration = acceleration || 0.1;
	time = time || 12;

	var y = getTop();

	var speed = 1 + acceleration;
	window.scrollTo(0, Math.floor(y / speed));

	if(y > 0) {
		var invokeFunction = "top.goTop(" + acceleration + ", " + time + ")"
		window.setTimeout(invokeFunction, time);
	}

	console.log(y);
	return false;
}

function scrollTop(){
	var a = document.getElementById('gotop');

	if( getTop() > 300 ){
		a.style.display = 'block';
	} else {
		a.style.display = 'none';
	}

	return false;
}

function getTop() {
	return window.scrollY // Chrome, Firefox
				|| window.pageYOffset // Modern IE, including IE11
				|| document.documentElement.scrollTop // Old IE, 6,7,8)
}

if (window.addEventListener){
	window.addEventListener("scroll", scrollTop, false);
}
else if (window.attachEvent){
	window.attachEvent("onscroll", scrollTop);
}	

