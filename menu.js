function showMenu(event) {
	event.preventDefault();
	var navs = document.querySelectorAll("#catalog-menu .nav-header");
	var actives = document.querySelectorAll("#catalog-menu .active");

	for (var i = 0; i < actives.length; i++) {
		if (actives[i].classList.contains("active")) actives[i].classList.remove("active");
	}

	this.classList.add("active");
	this.children[0].classList.add("active");
}

function hideMenu() {
	var actives = document.querySelectorAll("#catalog-menu .active");

	for (var i = 0; i < actives.length; i++) {
		if (actives[i].classList.contains("active")) actives[i].classList.remove("active");
	}
}

//	Функции для работы с DOM
//	getElementById
//	getElementsByClassName
//	getElementsByTagName
//	document.querySelectorAll('ul > li:last-child');
